FROM debian:latest
COPY . /web
WORKDIR /web
RUN chmod +x ./scripts/docker/install.sh
RUN ./scripts/docker/install.sh
RUN composer install -n
EXPOSE 8000
CMD php bin/console server:run 0.0.0.0:8000